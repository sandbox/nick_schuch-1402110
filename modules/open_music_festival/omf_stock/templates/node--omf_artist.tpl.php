<?php
  //Get the page elements for rendering
  $body = render($content['body']);
  $banner = render($content['field_omf_artist_banner']);
  $small_image = render($content['field_omf_common_images']);
  $video = render($content['field_omf_common_video']);
  $remote_url = render($content['field_omf_common_remote_url']);
  $fb_url = render($content['field_omf_common_fb_url']);
  $fb_url_raw = $content['field_omf_common_fb_url']['#items']['0']['url'];
  $youtube_url = render($content['field_omf_common_youtube_url']);
  $twitter_url = render($content['field_omf_common_twitter_url']);
  $myspace_url = render($content['field_omf_common_myspace_url']);
  $music_player = render($content['field_omf_artist_music_tracks']);
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  
  <?php if (!$page && $title): ?>
  <header>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php print render($title_suffix); ?>
  </header>
  <?php endif; ?>
  
  <div<?php print $content_attributes; ?>>
    <div id="artist-about-column" class="alpha grid-10">
      <?php
        //Render the about elements of the artist
        if ($banner) {
          print $banner;
          ?><div id="omf-banner-title"> <?php print $title; ?></div><?php
        }
        if ($body) {
          print $body;
        }
      ?>
    </div>
    
    <div id="artist-media-column" class="omega grid-6">
      <div class="artist-social-links">
        <?php
          //Facebook icon
          if ($fb_url) {
            print $fb_url;
          }
          //Youtube icon
          if ($youtube_url) {
            print $youtube_url;
          }
          //Twitter icon
          if ($twitter_url) {
            print $twitter_url;
          }
          //Myspace icon
          if ($myspace_url) {
            print $myspace_url;
          }
          //Home icon
          if ($remote_url) {
            print $remote_url;
          }
        ?>
      </div>
      <div class="artist-media">
        <?php
          //Render the media elements of the artist
          if ($music_player) {
            print $music_player;
          }

          ?>

          <div class="fb-like-box"
               data-href="<?php echo $fb_url_raw; ?>"
               data-width="<?php echo _omf_artists_get_omega_width(6); ?>%"
               data-show-faces="false"
               data-stream="true"
               data-header="false"
	       data-border-color="black">
          </div>
          
          <?php
          
          if ($video) {
            print $video;
          }
        ?>
      </div>
    </div>
  </div>
</article>
