<?php
  //Get the page elements for rendering
  $body = render($content['body']);
  $small_image = render($content['field_omf_common_images']);
  $remote_url = render($content['field_omf_common_remote_url']);
  $fb_url = render($content['field_omf_common_fb_url']);
  $youtube_url = render($content['field_omf_common_youtube_url']);
  $twitter_url = render($content['field_omf_common_twitter_url']);
  $myspace_url = render($content['field_omf_common_myspace_url']);
?>

<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  
  <?php if (!$page && $title): ?>
  <header>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php print render($title_suffix); ?>
  </header>
  <?php endif; ?>
  
  <div<?php print $content_attributes; ?>>
      <?php
	if ($body) { 
          print $body; 
        } ?>
      <div id="artist-social-links">
        <?php
          //Facebook icon
          if ($fb_url) {
            print $fb_url;
          }
          //Youtube icon
          if ($youtube_url) {
            print $youtube_url;
          }
          //Twitter icon
          if ($twitter_url) {
            print $twitter_url;
          }
          //Myspace icon
          if ($myspace_url) {
            print $myspace_url;
          }
          //Home icon
          if ($remote_url) {
            print $remote_url;
          }
        ?>
      </div>
    </div>
  </div>
</article>
