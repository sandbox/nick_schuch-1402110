<?php
/**
 * @file
 * omf_sponsor.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function omf_sponsor_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function omf_sponsor_node_info() {
  $items = array(
    'omf_sponsor' => array(
      'name' => t('Sponsor'),
      'base' => 'node_content',
      'description' => t('A sponsor which will receive acknowledgement on this festival site for there contribution.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
