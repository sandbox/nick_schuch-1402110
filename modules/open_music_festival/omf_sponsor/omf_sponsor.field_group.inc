<?php
/**
 * @file
 * omf_sponsor.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function omf_sponsor_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_omf_sponsor_social_media|node|omf_sponsor|form';
  $field_group->group_name = 'group_omf_sponsor_social_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'omf_sponsor';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social Media',
    'weight' => '13',
    'children' => array(
      0 => 'field_omf_common_youtube_url',
      1 => 'field_omf_common_fb_url',
      2 => 'field_omf_common_twitter_url',
      3 => 'field_omf_common_myspace_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_omf_sponsor_social_media|node|omf_sponsor|form'] = $field_group;

  return $export;
}
