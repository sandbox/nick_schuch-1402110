<?php
/**
 * @file
 * omf_product_commerce.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function omf_product_commerce_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function omf_product_commerce_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function omf_product_commerce_node_info() {
  $items = array(
    'omf_product_display' => array(
      'name' => t('Product Display'),
      'base' => 'node_content',
      'description' => t('Products (tickets etc) to be displayed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
