<?php
/**
 * @file
 * omf_artists.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function omf_artists_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-omf-common-footer-menu
  $menus['menu-omf-common-footer-menu'] = array(
    'menu_name' => 'menu-omf-common-footer-menu',
    'title' => 'Footer',
    'description' => 'A menu with footer links.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('A menu with footer links.');
  t('Footer');
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
