<?php
/**
 * @file
 * omf_artists.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function omf_artists_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:facilities
  $menu_links['main-menu:facilities'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'facilities',
    'router_path' => 'facilities',
    'link_title' => 'Facilities',
    'options' => array(
      'attributes' => array(
        'title' => 'The facilities available at the venue.',
        'id' => 'omf-main-menu-facilities',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: main-menu:faq
  $menu_links['main-menu:faq'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'faq',
    'router_path' => 'faq',
    'link_title' => 'FAQ',
    'options' => array(
      'attributes' => array(
        'title' => 'Frequently ask questions about the event.',
        'id' => 'omf-main-menu-faq',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: main-menu:lineup
  $menu_links['main-menu:lineup'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'lineup',
    'router_path' => 'lineup',
    'link_title' => 'Lineup',
    'options' => array(
      'attributes' => array(
        'title' => 'The lineup for the event',
        'id' => 'omf-main-menu-lineup',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: main-menu:node/10
  $menu_links['main-menu:node/10'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/10',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'attributes' => array(
        'id' => 'omf-main-menu-about',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: main-menu:node/11
  $menu_links['main-menu:node/11'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/11',
    'router_path' => 'node/%',
    'link_title' => 'Contact Us',
    'options' => array(
      'attributes' => array(
        'id' => 'omf-main-menu-contact',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('Contact Us');
  t('FAQ');
  t('Facilities');
  t('Lineup');


  return $menu_links;
}
