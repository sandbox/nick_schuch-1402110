<?php
  //Get the page variables
  $fb_url = variable_get('omf_common_facebook_url_3');
  $grid_width = variable_get('omf_common_facebook_grid_width_3');
  $px_height = variable_get('omf_common_facebook_px_height_3');
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div class="fb-like-box"
     data-href="<?php echo $fb_url; ?>"
     data-width="<?php echo _omf_artists_get_omega_width($grid_width); ?>%"
     <?php
      if ($px_height) {
        echo "data-height='$px_height'";
      }
     ?>
     data-show-faces="false"
     data-colorscheme="dark"
     data-stream="true"
     data-header="false"
     data-border-color="black">
</div>
