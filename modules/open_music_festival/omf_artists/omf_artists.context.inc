<?php
/**
 * @file
 * omf_artists.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function omf_artists_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'omf_artist_featured_context';
  $context->description = 'The context for the featured artist page.';
  $context->tag = 'omf artist';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'omf_artists-omf_common_social' => array(
          'module' => 'omf_artists',
          'delta' => 'omf_common_social',
          'region' => 'front_facebook',
          'weight' => '-16',
        ),
        'omf_artists-omf_common_facebook' => array(
          'module' => 'omf_artists',
          'delta' => 'omf_common_facebook',
          'region' => 'front_facebook',
          'weight' => '-15',
        ),
        'views-358766dbaecf46b0b7c46612e8a7d2d9' => array(
          'module' => 'views',
          'delta' => '358766dbaecf46b0b7c46612e8a7d2d9',
          'region' => 'front_images_carousel',
          'weight' => '-10',
        ),
        'views-omf_common_featured_video-block' => array(
          'module' => 'views',
          'delta' => 'omf_common_featured_video-block',
          'region' => 'front_video_carousel',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'omf_artist_featured_delta',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('The context for the featured artist page.');
  t('omf artist');
  $export['omf_artist_featured_context'] = $context;

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'omf_common_about_context';
  $context->description = 'Context for the About page.';
  $context->tag = 'omf common';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about' => 'about',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'omf_artists-omf_common_social' => array(
          'module' => 'omf_artists',
          'delta' => 'omf_common_social',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'omf_artists-omf_common_facebook' => array(
          'module' => 'omf_artists',
          'delta' => 'omf_common_facebook',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'boxes-omf_common_about_location' => array(
          'module' => 'boxes',
          'delta' => 'omf_common_about_location',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'omf_common_about',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context for the About page.');
  t('omf common');
  $export['omf_common_about_context'] = $context;

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'omf_common_contact_us_context';
  $context->description = 'Context for the Contact Us page.';
  $context->tag = 'omf common';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'omf_artists-omf_common_social' => array(
          'module' => 'omf_artists',
          'delta' => 'omf_common_social',
          'region' => 'sidebar_second',
          'weight' => '-18',
        ),
        'omf_artists-omf_common_facebook_2' => array(
          'module' => 'omf_artists',
          'delta' => 'omf_common_facebook_2',
          'region' => 'sidebar_second',
          'weight' => '-17',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'omf_common_contact_us',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context for the Contact Us page.');
  t('omf common');
  $export['omf_common_contact_us_context'] = $context;

  return $export;
}
