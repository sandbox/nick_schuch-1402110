<?php
/**
 * @file
 * omf_artists.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function omf_artists_taxonomy_default_vocabularies() {
  return array(
    'omf_common_content_status' => array(
      'name' => 'Content Status',
      'machine_name' => 'omf_common_content_status',
      'description' => 'Status that can be applied to Content and its Fields.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
