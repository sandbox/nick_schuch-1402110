<?php
/**
 * @file
 * omf_artists.box.inc
 */

/**
 * Implements hook_default_box().
 */
function omf_artists_default_box() {
  $export = array();

  $box = new stdClass;
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'omf_common_about_location';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Location of the Open Music Festival';
  $box->options = array(
    'body' => array(
      'value' => '<iframe height="300" width="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=bundaberg&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=56.06887,87.539063&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=Bundaberg+Queensland,+Australia&amp;t=m&amp;z=14&amp;ll=-24.864963,152.348653&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=bundaberg&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=56.06887,87.539063&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=Bundaberg+Queensland,+Australia&amp;t=m&amp;z=14&amp;ll=-24.864963,152.348653" style="color:#0000FF;text-align:left">View Larger Map</a></small>',
      'format' => 'full_html',
    ),
  );
  $export['omf_common_about_location'] = $box;

  return $export;
}
