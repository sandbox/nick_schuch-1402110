<?php
/**
 * @file
 * omf_artists.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function omf_artists_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_omf_artist_media|node|omf_artist|form';
  $field_group->group_name = 'group_omf_artist_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'omf_artist';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Media',
    'weight' => '6',
    'children' => array(
      0 => 'field_omf_artist_banner',
      1 => 'field_omf_common_images',
      2 => 'field_omf_common_video',
      3 => 'field_omf_artist_music_tracks',
      4 => 'field_omf_artist_thumbnail',
      5 => 'field_omf_common_feature_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_omf_artist_media|node|omf_artist|form'] = $field_group;

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_omf_artist_social_media|node|omf_artist|form';
  $field_group->group_name = 'group_omf_artist_social_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'omf_artist';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social Media',
    'weight' => '5',
    'children' => array(
      0 => 'field_omf_common_fb_url',
      1 => 'field_omf_common_youtube_url',
      2 => 'field_omf_common_twitter_url',
      3 => 'field_omf_common_myspace_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_omf_artist_social_media|node|omf_artist|form'] = $field_group;

  return $export;
}
