<?php
/**
 * @file
 * omf_faq.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function omf_faq_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'omf_faq_listing';
  $view->description = 'Listing of all FAQ\'s.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'FAQ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'FAQ';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Question */
  $handler->display->display_options['fields']['field_omf_faq_question']['id'] = 'field_omf_faq_question';
  $handler->display->display_options['fields']['field_omf_faq_question']['table'] = 'field_data_field_omf_faq_question';
  $handler->display->display_options['fields']['field_omf_faq_question']['field'] = 'field_omf_faq_question';
  $handler->display->display_options['fields']['field_omf_faq_question']['label'] = '';
  $handler->display->display_options['fields']['field_omf_faq_question']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_omf_faq_question']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_omf_faq_question']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_omf_faq_question']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_omf_faq_question']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_omf_faq_question']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_omf_faq_question']['field_api_classes'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_type'] = 'h2';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="omf-faq-question clearfix">
  <div class="alpha grid-1">
    <h2>Q</h2>
  </div>
  <div class="omega grid-15">
    [field_omf_faq_question]
  </div>
</div>
<div class="omf-faq-answer clearfix">
  <div class="alpha grid-1">
    <h2>A</h2>
  </div>
  <div class="omega grid-15">
    [body]
  </div>
</div>';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'omf_faq' => 'omf_faq',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'faq';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'FAQ';
  $handler->display->display_options['menu']['description'] = 'Frequently Asked Questions';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['omf_faq_listing'] = $view;

  return $export;
}
