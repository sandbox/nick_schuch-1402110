<?php
/**
 * @file
 * omf_faq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function omf_faq_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function omf_faq_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function omf_faq_node_info() {
  $items = array(
    'omf_faq' => array(
      'name' => t('FAQ'),
      'base' => 'node_content',
      'description' => t('Frequently asked questions are asked about your Festival.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
