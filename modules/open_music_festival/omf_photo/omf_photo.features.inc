<?php
/**
 * @file
 * omf_photo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function omf_photo_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function omf_photo_node_info() {
  $items = array(
    'omf_photo' => array(
      'name' => t('Photo'),
      'base' => 'node_content',
      'description' => t('Miscellaneous photos that can be promoted to the front page (as well as artist content).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
