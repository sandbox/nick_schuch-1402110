<?php
/**
 * @file
 * omf_video.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function omf_video_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function omf_video_node_info() {
  $items = array(
    'omf_video' => array(
      'name' => t('Video'),
      'base' => 'node_content',
      'description' => t('Miscellaneous videos that can also be promoted to the front page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
