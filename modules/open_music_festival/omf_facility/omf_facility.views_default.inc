<?php
/**
 * @file
 * omf_facility.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function omf_facility_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'omf_facilities_listing';
  $view->description = 'A list of all the facilities the event has to offer.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Facilities';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Facilities';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Images */
  $handler->display->display_options['fields']['field_omf_common_images']['id'] = 'field_omf_common_images';
  $handler->display->display_options['fields']['field_omf_common_images']['table'] = 'field_data_field_omf_common_images';
  $handler->display->display_options['fields']['field_omf_common_images']['field'] = 'field_omf_common_images';
  $handler->display->display_options['fields']['field_omf_common_images']['label'] = '';
  $handler->display->display_options['fields']['field_omf_common_images']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_omf_common_images']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_omf_common_images']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_omf_common_images']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_omf_common_images']['settings'] = array(
    'image_style' => 'os-grid2',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_omf_common_images']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_omf_common_images']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_omf_common_images']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_omf_common_images']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_omf_common_images']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="omf-facility-item clearfix">
  <div class="alpha grid-2">
    [field_omf_common_images]
  </div>
  <div class="omega grid-14">
    <h2>[title]</h2>
    <p>[body]</p>
  </div>
</div>';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'omf_facility' => 'omf_facility',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'facilities';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Facilities';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['omf_facilities_listing'] = $view;

  return $export;
}
