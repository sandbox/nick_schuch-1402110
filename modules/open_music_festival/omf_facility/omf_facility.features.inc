<?php
/**
 * @file
 * omf_facility.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function omf_facility_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function omf_facility_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function omf_facility_node_info() {
  $items = array(
    'omf_facility' => array(
      'name' => t('Facility'),
      'base' => 'node_content',
      'description' => t('A facility that will be provided on the day of the event.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
