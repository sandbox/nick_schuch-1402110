 core = 7.x
api = 2




; Modules - Contrib
projects[admin][subdir] = "contrib"
projects[admin][version] = "2.0-beta3"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.0-rc1"

projects[commerce][subdir] = "contrib"
projects[commerce][version] = "1.1"

projects[context][subdir] = "contrib"
projects[context][version] = "3.0-beta2"

projects[addressfield][subdir] = "contrib"
projects[addressfield][version] = "1.0-beta2"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.1"

projects[link][subdir] = "contrib"
projects[link][version] = "1.0"

projects[file_entity][type] = "module"
projects[file_entity][download][type] = "git"
projects[file_entity][download][url] = "git://drupalcode.org/project/file_entity.git"
projects[file_entity][download][revision] = "44a35a59eec42481e266ecaa185798f1287f48ef"
projects[file_entity][subdir] = "contrib"

projects[jplayer][subdir] = "contrib"
projects[jplayer][version] = "2.x-dev"

projects[media][subdir] = "contrib"
projects[media][version] = "1.0-rc2"

projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "1.0-alpha5"

projects[adaptive_image][subdir] = "contrib"
projects[adaptive_image][version] = "1.3"

projects[boxes][subdir] = "contrib"
projects[boxes][version] = "1.0-beta6"

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = "1.2"

projects[defaultcontent][subdir] = "contrib"
projects[defaultcontent][version] = "1.0-alpha4"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.0-rc1"

projects[imagecrop][subdir] = "contrib"
projects[imagecrop][version] = "1.0-rc3"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "1.0"

projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = "1.0-rc1"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0-beta4"

projects[radioactivity][subdir] = "contrib"
projects[radioactivity][version] = "2.0-beta2"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.0"

projects[styles][subdir] = "contrib"
projects[styles][version] = "2.0-alpha8"

projects[delta][subdir] = "contrib"
projects[delta][version] = "3.0-beta9"

projects[omega_tools][subdir] = "contrib"
projects[omega_tools][version] = "3.0-rc3"

projects[jcarousel][subdir] = "contrib"
projects[jcarousel][version] = "2.6"

projects[views][subdir] = "contrib"
projects[views][version] = "3.0-rc3"

projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "3.0"

projects[commerce_paypal][subdir] = "contrib"
projects[commerce_paypal][version] = "1.x-dev"





; Themes
projects[omega][type] = "theme"
projects[omega][version] = "3.0"